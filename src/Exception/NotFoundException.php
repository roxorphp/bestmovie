<?php

namespace App\Exception;

class NotFoundException extends \Exception implements JsonExceptionInterface
{
	
	function __construct($message){
	
		http_response_code(404);
		
		parent::__construct($message);

	}
	
}