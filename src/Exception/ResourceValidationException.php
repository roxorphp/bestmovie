<?php

namespace App\Exception;

class ResourceValidationException extends \Exception  implements JsonExceptionInterface
{

	function __construct($errors){
	
		http_response_code(422);
		
		$message = 'The JSON sent contains invalid data. Here are the errors you need to correct: ';
		if(is_string($errors)){
			$message .= $errors;
		}
		else{
			foreach ($errors as $error) {
				$message .= sprintf("Field %s: %s ", $error->getPropertyPath(), $error->getMessage());
			}
		}
		
		parent::__construct($message);

	}
	
}