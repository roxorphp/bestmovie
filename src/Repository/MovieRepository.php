<?php // src/Repository/UserRepository.php
namespace App\Repository;

use App\Entity\Movie;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MovieRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Movie::class);
    }
	
	/**
	 * Retourne les films choisi par un utilisateur
	 * @param User $user
	 * @return Movie[]
	 */ 
	public function findAllChosenBy(User $user){
		$qb = $this->createQueryBuilder('m')
		     ->innerjoin('m.userMovies', 'um')
			 ->andWhere('um.user = :user')
			 ->setParameter('user',$user)
			 ->getQuery();
	
        return $qb->execute();
	}
	
    /**
	 * Retourne le film ayant obtenu le plus de votes
     * @return Movie|null
     */
    public function getBest()
    {
        $qb = $this->createQueryBuilder('m')
	              ->join('m.userMovies', 'um')
				  ->select('count(um.movie) total, m.title , m.poster')
				  ->addGroupby('um.movie')
				  ->orderBy('total', 'DESC')
				  ->getQuery();
				  
		return $qb->setMaxResults(1)->getOneOrNullResult();
    }
}