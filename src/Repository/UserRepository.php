<?php // src/Repository/UserRepository.php
namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }
	
    /**
	 * Retourne les utilisateurs ayant choisit au moins un film
     * @return array
     */
    public function findAllHavingChoices(): array
    {

        $qb = $this->createQueryBuilder('u')
                   ->select('u.id, u.name ,u.email , u.birthday')
				   ->innerJoin('u.userMovies','um')
				   ->addGroupby('u.id')
                   ->getQuery();
	
        return $qb->execute();
		
    }
}