<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserMovie;
use App\Entity\Movie;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Exception\ResourceValidationException;
use App\Exception\NotFoundException;

class UserController extends Controller
{
    /**
	 * Création d'un nouvel utilisateur
     * @Route("/users")
	 * @Method({"POST"})
	 * @param Request $request
	 * @return string
     */
    public function createAction(Request $request)
    {
		$user = new User;

		$data = $this->getDataFromHeader($request);

		$user->setName($data['name'])
		     ->setEmail($data['email'])
			 ->setBirthday(new \DateTime($data['birthday']));

		$this->checkIfValid($user);

		$em = $this->getDoctrine()->getManager();
		$em->persist($user);
		$em->flush();

		return $this->json($user);

    }

    /**
	 * Ajouter un choix de film
	 * Exemple de contenu du body: {"movieTitle" : "Rambo"}
     * @Route("/users/{id}/movies")
     * @Method({"POST"})
	 * @param Request $request
	 * @param User $user - utilisateur associé au choix du film
	 * @return string
     */
    public function addMovieAction(Request $request, User $user = null)
    {

		if($user === null){
			throw new NotFoundException("This user doesn't exists");
		}

		if($user->getUserMovies()->count() === 3){
			throw new ResourceValidationException("An user cannot chose more than 3 movies");
		}

		$data = $this->getDataFromHeader($request);

		$movie = $this->getMovieFromTitle($data["movieTitle"]);

		$userMovie = new UserMovie();

		$userMovie->setMovie($movie)
		          ->setUser($user);

		$this->checkIfValid($userMovie);

		$em = $this->getDoctrine()->getManager();

		$em->persist($userMovie);

		$em->flush();

		return $this->json(['response' => "success" , "message" => "Film choice successefully added!"]);
    }

	/**
	  * @Route("users/{user}/movies/{movie}")
      *	@Method({"DELETE"})
	  * @param User $user - Utilisateur dont un des choix va être supprimé
	  * @param Movie $movie - Film associé aux choix de l'utilisateur à supprimer
	  * @return string
	  */
	public function deleteAction(User $user = null,Movie $movie = null){

		if($user === null){
			throw new NotFoundException("This user doesn't exists");
		}

		if($movie === null){
			throw new NotFoundException("This movie doesn't exists");
		}

		$em = $this->getDoctrine()->getManager();

		$userMovieRepository = $em->getRepository(UserMovie::class);
		$userMovie = $userMovieRepository->findOneBy(['user' => $user, 'movie' => $movie]);

		if($userMovie === null){
			throw new ResourceValidationException("{$user->getName()} doesn't have choice the movie {$movie->getTitle()}");
		}

		$em->remove($userMovie);

		$em->flush();
		return $this->json(['response' => "success" , "message" => "Film choice successefully removed!"]);
	}

	/**
	 * Retourne la liste des films choisis par un utilisateur
	 * @Route("/users/{id}/movies")
	 * @Method({"GET"})
	 * @param User $user - utilisateur dont on souhaite afficher les choix
	 * @return string
	 */
	public function showMoviesAction(User $user = null){
		if($user === null){
			throw new NotFoundException("This user doesn't exists");
		}

		$em = $this->getDoctrine()->getManager();
		$movieRepository = $em->getRepository(Movie::class);
		$movies = $movieRepository->findAllChosenBy($user);
		$code = count($movies) ===0 ? 204 : 200;

		return $this->json($movies,$code);
	}

	/**
	 * Retourne la liste des utilisateurs ayant choisi au moins un film
	 * @Route("/users/having-movies")
	 * @Method({"GET"})
	 * @return string
	 */
	public function showHavingChoicesAction(){

		$em = $this->getDoctrine()->getManager();
		$userRepository = $em->getRepository(User::class);
		$users = $userRepository->findAllHavingChoices();
		$code = count($users) ===0 ? 204 : 200;
		return $this->json($users,$code);
	}

	/**
	 * Vérifie si une entité est valide
	 * @return bool
	 * @throws ResourceValidationException if operation fail
	 */
	protected function checkIfValid($entity){

		$validator = $this->get('validator');

		$errors = $validator->validate($entity);

		if (count($errors) > 0) {
			throw new ResourceValidationException($errors);
		}
		return true;
	}

	/**
	 * Retourne un objet Movie et rajoute éventuellement une entrée supplémentaire dans la table movie si celle-ci n'y figurait pas
	 * @param string $title - Titre du film
	 * @return Movie
     */
	protected function getMovieFromTitle(string $title){
		$em = $this->getDoctrine()->getManager();
		$movie = $em->getRepository(Movie::class)->findOneByTitle($title);

		if(!$movie){
			$dataMovie = $this->getFilmDataFromOMDBApi($title);
			$movie = new Movie;
			$movie->setTitle($title);
			$movie->setPoster($dataMovie['Poster']);
			$em->persist($movie);
			$em->flush();
		}
		return $movie;
	}

	/**
	 * Extrait les données d'un film à partir de l'API OMDB en fonction du titre
	 * @param string $title - Titre du film
	 * @return array
	 * @throws NotFoundException if operation fail
	 */
	protected function getFilmDataFromOMDBApi($title){
		$title = urlencode($title);
        if($title == 404){
			throw new NotFoundException("This movie cannot be chosen because of its title (even if it exists!).");
		}    
        $data = json_decode(file_get_contents("http://www.omdbapi.com/?t=$title&apikey=a2f5cba9"),true);
		$code = $this->getHttpCode($http_response_header);

		if($code != 200 || $data['Response'] === "False"){
			throw new NotFoundException("The film \"$title\" doesn't exists");
		}

		return $data;
	}

	/** Retourne le code de la réponse HTTP
	 * @param array $http_response_header
	 * @return int
	 **/
	protected function getHttpCode($http_response_header)
	{
		if(is_array($http_response_header))
		{
			$parts=explode(' ',$http_response_header[0]);
			if(count($parts)>1) {
				return intval($parts[1]);
			}
		}
		return 0;

	}

	/** Récupère le JSON contenu dans le body du header HTTP et le retourne sous la forme d'un tableau
	 *  @param $request
	 *  @return array
	 */
	protected function getDataFromHeader(Request $request){
		return json_decode($request->getContent(),true);
	}

}
