<?php

namespace App\Controller;

use App\Entity\Movie;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Exception\ResourceValidationException;
use Exception;

class MovieController extends Controller
{

	/**
	 * Retourne ne film ayant obtenu le plus de votes
	 * @Route("/movies/best")
	 * @Method({"GET"})
	 * @return string
	 */
	public function showBestAction(){
		$em = $this->getDoctrine()->getManager();
		$movieRepository = $em->getRepository(Movie::class);
		$movie = $movieRepository->getBest();
		unset($movie['total']);
		$code = $movie === null  ? 204 : 200;
		return $this->json($movie,$code);
	}

}