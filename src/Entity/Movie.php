<?php namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

	/**
     * @Assert\NotBlank()
     * @ORM\Column(unique=true)
     */
    private $title;
	
	/**  
     * @Assert\NotBlank()
	 * @ORM\Column
	 */
	private $poster;
	
	/**
     * @ORM\OneToMany(targetEntity="App\Entity\UserMovie", mappedBy="movie")
     */
	private $userMovies;
	
    public function setTitle($title)
    {
    	$this->title = $title;
    }

    public function getTitle()
    {
    	return $this->title;
    }
	
    public function setPoster($poster)
    {
    	$this->poster = $poster;
    }

    public function getPoster()
    {
    	return $this->poster;
    }
	
}