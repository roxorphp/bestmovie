<?php namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Exception\ResourceValidationException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks() 
 * @UniqueEntity("email")
 * @UniqueEntity("name")
 */
class User
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

	/**
	 * @var string
     * @Assert\NotBlank()
     * @ORM\Column(length=30, unique=true)
     */
    private $name;
	
	/**
	 * @var string
     * @Assert\NotBlank()
	 * @Assert\Email()
     * @ORM\Column(length=140, unique=true)
     */
    private $email;
	
	/**  
	 * @var \Date
     * @Assert\NotBlank()
	 * @ORM\Column(name="birthday", type="date")  
	 * @Assert\Date()
	 */
	private $birthday;
	
	/**
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;  
	
	/**
     * @ORM\OneToMany(targetEntity="App\Entity\UserMovie", mappedBy="user")
     */
	private $userMovies;
	
	public function __construct()
	{
		$this->movies = new ArrayCollection();
	}
	
    public function getId()
    {
    	return $this->id;
    }
	
    public function setName($name)
    {
    	$this->name = $name;
		return $this;
    }

    public function getName()
    {
    	return $this->name;
    }
	
    public function setEmail($email)
    {
    	$this->email = $email;
		return $this;
    }

    public function getEmail()
    {
    	return $this->email;
    }
	
    public function setBirthday($birthday)
    {
    	$this->birthday = $birthday;
		return $this;
    }

    public function getBirthday()
    {
    	return $this->birthday;
    }
	
	/**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return SELF
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }
	
    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
		return $this->createdAt;
    }
	
    public function addUserMovie(Movie $userMovie)
    {
		if($this->userMovies->contains($userMovie)){
			throw new ResourceValidationException("This movie has already been chosen by $this->name");
		}
		$this->userMovies[] = $userMovie;

		return $this;
    }

    public function removeUserMovie(Movie $userMovie)
    {
		if(!$this->userMovies->contains($userMovie)){
			throw new ResourceValidationException("This movie hasn't been chosen by $this->name");
		}
		$this->userMovies->removeElement($userMovie);

		return $this;
    }

	/**
	 * @return UserMovie[]
	 */
    public function getUserMovies()
    {
		return $this->userMovies;
    }
	
	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
    { 
		$this->setCreatedAt(new \Datetime());
	}
}