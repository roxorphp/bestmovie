<?php namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity
 * @UniqueEntity(
        fields={"user","movie"},
		message="This movie has already been chosen by this user"
		)
 */
class UserMovie
{

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy = "userMovies")
	 */
	private $user;
	
	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="App\Entity\Movie", inversedBy = "userMovies")
	 */
	private $movie;

	/** 
	 * @return SELF
	 */
    public function setUser(User $user)
    {
    	$this->user = $user;
		return $this;
    }
	
	/** 
	 * @return User
	 */
    public function getUser()
    {
    	return $this->user;
    }
	
	/** 
	 * @return SELF
	 */
    public function setMovie(Movie $movie)
    {
    	$this->movie = $movie;
		return $this;
    }
	
	/** 
	 * @return Movie
	 */    
	public function getMovie()
    {
    	return $this->movie;
    }
	
}